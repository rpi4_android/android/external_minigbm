/*
 * Copyright 2019 The FydeOS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * Author: Yang Tsao <yang@fydeos.io>
 */

#ifdef DRV_V3D

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <xf86drm.h>

#include "drv_priv.h"
#include "helpers.h"
#include "util.h"
#include "v3d_drm.h"

static const uint32_t render_target_formats[] = { DRM_FORMAT_RGB565, DRM_FORMAT_BGR565, 
	DRM_FORMAT_ARGB8888, DRM_FORMAT_XRGB8888, DRM_FORMAT_ABGR8888, DRM_FORMAT_XBGR8888 };

static int v3d_init(struct driver *drv) {
  drv_add_combinations(drv, render_target_formats, ARRAY_SIZE(render_target_formats),
           &LINEAR_METADATA, BO_USE_RENDER_MASK );
  drv_modify_combination(drv, DRM_FORMAT_ARGB8888, &LINEAR_METADATA, BO_USE_CURSOR | BO_USE_SCANOUT);
  return 0;  
}

static int v3d_bo_create_for_modifiers(struct bo *bo, uint32_t width, uint32_t height,
               uint32_t format, uint64_t modifier) {
  int ret;
  size_t plane;
  uint32_t stride;
  struct drm_v3d_create_bo bo_create;
  memset(&bo_create, 0, sizeof(bo_create));

  stride = drv_stride_from_format(format, width, 0);
  //stride = ALIGN(stride, 64);
  drv_bo_from_format(bo, stride, height, format);
  bo_create.size = bo->total_size;
  bo->format_modifiers[0] = modifier;

  ret = drmIoctl(bo->drv->fd, DRM_IOCTL_V3D_CREATE_BO, &bo_create);
  if (ret) {
    drv_log("DRM_IOCTL_V3D_CREATE_BO failed (size=%zu)\n", bo->total_size);
    return -errno;
  }

  for (plane = 0; plane < bo->num_planes; plane)
    bo->handles[plane].u32 = bo_create.handle;
  return 0;
}

static int v3d_bo_create_with_modifiers(struct bo *bo, uint32_t width, uint32_t height,
           uint32_t format, const uint64_t *modifiers, uint32_t count) {
  return v3d_bo_create_for_modifiers(bo, width, height, format, DRM_FORMAT_MOD_LINEAR/*modifier*/);
}

static int v3d_bo_create(struct bo *bo, uint32_t width, uint32_t height, uint32_t format,
       uint64_t use_flags) {
  return v3d_bo_create_for_modifiers(bo, width, height, format, DRM_FORMAT_MOD_LINEAR/*modifier*/);
}

static void *v3d_bo_map(struct bo *bo, struct vma *vma, size_t plane, uint32_t map_flags) {
  int ret;
  struct drm_v3d_mmap_bo bo_map;
  memset(&bo_map, 0, sizeof(bo_map));
  bo_map.handle = bo->handles[0].u32;

  ret = drmIoctl(bo->drv->fd, DRM_IOCTL_V3D_MMAP_BO, &bo_map);
  if (ret) {
    drv_log("DRM_V3D_MMAP_BO failed\n");
    return MAP_FAILED;
  }
  vma->length = bo->total_size;
  return mmap(NULL, bo->total_size, drv_get_prot(map_flags), MAP_SHARED, bo->drv->fd,
        bo_map.offset);
}

const struct backend backend_v3d = {
  .name = "v3d",
  .init = v3d_init,
  .bo_create = v3d_bo_create,
  .bo_create_with_modifiers = v3d_bo_create_with_modifiers,
  .bo_import = drv_prime_bo_import,
  .bo_destroy = drv_gem_bo_destroy,
  .bo_map = v3d_bo_map,
  .bo_unmap = drv_bo_munmap,
};

#endif // DEV_V3D
